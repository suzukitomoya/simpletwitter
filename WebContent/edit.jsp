<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>編集</title>
    <link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<c:if test="${ not empty errorMessages }">
		<div class="errorMessages">
			<ul>
				<c:forEach items="${errorMessages}" var="errorMessage">
					<li><c:out value="${errorMessage}" />
				</c:forEach>
			</ul>
		</div>
		<c:remove var="errorMessages" scope="session" />
	</c:if>

	<div class="form-area">
		<form action="edit" method="post">
			<input name="id" value="${message.id}" id="id" type="hidden"/>
			いま、どうしてる？<br />
			<textarea name="text" rows="5" cols="100" class="tweet-box">${message.text}</textarea>
			<br />
			<input type="submit" value="更新">(140文字まで)
		</form>
	</div>

    <div class="copyright"> Copyright(c)鈴木智也</div>
</body>
</html>